#include "ltrmodule_fpga_update.h"
#include "ltr/include/ltr25api.h"
#include <stdlib.h>


static INT f_mod_open(void** hnd, DWORD net_addr, WORD net_port, const char *csn, INT slot);
static INT f_mod_close(void* hnd);
static INT f_mod_fpga_wr_en(void* hnd, BOOL en);
static INT f_mod_erase(void* hnd, DWORD addr, DWORD size);
static INT f_mod_write(void* hnd, DWORD addr, const BYTE* data, DWORD size);
static INT f_mod_read(void* hnd, DWORD addr, BYTE* data, DWORD size);
static LPCSTR f_mod_err_str(void *hnd, INT err);
static LPCSTR f_mod_type_spec(void *hnd);

extern LTR25API_DllExport(INT) LTR25_FPGAFirmwareWriteEnable(TLTR25 *hnd, BOOL en) ;




static const char* f_type_specifiers[] = {
    "industrial",
    "commercial",
    NULL
};


t_module_info ltr25_info = {
    "LTR25",
    LTR_MID_LTR25,
    MODULE_FLAG_TYPE_SPEC_REQIRED,
    0x100000,
    0x100000 - 0x010000,
    8*1024,
    f_type_specifiers,
    f_mod_open,
    f_mod_close,
    f_mod_fpga_wr_en,
    f_mod_erase,
    f_mod_write,
    f_mod_read,
    f_mod_err_str,
    f_mod_type_spec
};




static INT f_mod_open(void** phnd, DWORD net_addr, WORD net_port, const char *csn, INT slot) {
    TLTR25* hnd = malloc(sizeof(TLTR25));
    INT err = hnd==NULL ? LTR_ERROR_MEMORY_ALLOC : LTR_OK;

    if (err==LTR_OK) {
        LTR25_Init(hnd);
        err = LTR25_Open(hnd, net_addr, net_port, csn, slot);
        /* Игнорируем некритические ошибки, которые нам все равно могут дать
         * обновить прошивку */
        if ((err != LTR_WARNING_MODULE_IN_USE) && (LTR25_IsOpened(hnd)==LTR_OK)) {
            err = LTR25_FPGAEnable(hnd, FALSE);
        }
    }

    if (err==LTR_OK) {
        *phnd = hnd;
    } else {
        free(hnd);
    }
    return err;
}

static INT f_mod_close(void* hnd) {
    INT err = LTR25_Close((TLTR25*)hnd);
    free(hnd);
    return err;
}

static INT f_mod_fpga_wr_en(void* hnd, BOOL en) {
    return LTR25_FPGAFirmwareWriteEnable((TLTR25*)hnd, en);
}

static INT f_mod_erase(void* hnd, DWORD addr, DWORD size) {
    return LTR25_FlashErase((TLTR25*)hnd, addr, size);
}

static INT f_mod_write(void* hnd, DWORD addr, const BYTE* data, DWORD size) {
    return LTR25_FlashWrite((TLTR25*)hnd, addr, data, size);
}

static INT f_mod_read(void* hnd, DWORD addr, BYTE* data, DWORD size) {
    return LTR25_FlashRead((TLTR25*)hnd, addr, data, size);
}

static LPCSTR f_mod_err_str(void *hnd, INT err) {
    return LTR25_GetErrorString(err);
}

static LPCSTR f_mod_type_spec(void *hnd) {
    return ((TLTR25*)hnd)->ModuleInfo.Industrial ? f_type_specifiers[0] : f_type_specifiers[1];
}
