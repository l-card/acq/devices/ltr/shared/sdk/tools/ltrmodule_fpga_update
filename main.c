﻿#include "ltr/include/ltr35api.h"
#include "ltr/include/ltrapi.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <ctype.h>
#include "getopt.h"
#include "ltrmodule_fpga_update.h"


#define OPT_HELP        'h'
#define OPT_VERBOSE     'v'
#define OPT_CRATE_SN    'c'
#define OPT_SLOT        's'
#define OPT_MODULE      'm'
#define OPT_MODULE_TYPE 't'
#define OPT_OPERATION   'o'
#define OPT_ALL          0x100


#define ERR_INVALID_USAGE           -1
#define ERR_OPEN_FILE               -2
#define ERR_FILE_READ               -3
#define ERR_UNSUP_FIRM_FILE         -4
#define ERR_MODULE_INFO             -5
#define ERR_FIRM_VERIFY             -6
#define ERR_EMPTY_SLOT              -7
#define ERR_IDENTIFYING_SLOT        -8
#define ERR_INVALID_MID_IN_SLOT     -9
#define ERR_MODULE_INVALID_TYPE     -10
#define ERR_OPS_WITH_ERRORS         -11


#define MAX_INFO_SIZE    128
#define MAX_INFO_OFFSET  0x80

#define OP_WRITE    0x01
#define OP_VERIFY   0x02
#define OP_READ     0x04



static const struct option f_long_opt[] = {
    {"help",         no_argument,       0, OPT_HELP},
    {"csn",          required_argument, 0, OPT_CRATE_SN},
    {"slot",         required_argument, 0, OPT_SLOT},
    {"module",       required_argument, 0, OPT_MODULE},
    {"type",         required_argument, 0, OPT_MODULE_TYPE},
    {"all",          no_argument,       0, OPT_ALL},
    {"op",           required_argument, 0, OPT_OPERATION},
    {0,0,0,0}
};

static const char* f_opt_str = "hc:s:m:o:t:";

typedef struct {
    int verbose;
    int all;
    int slot;
    const char *csn;
    const char *filename;
    const char *mod_type_spec;
    t_module_info *module;
} t_ltrcfg_state;


extern t_module_info ltr35_info;
extern t_module_info ltr25_info;

static t_module_info* f_modules[] = {
    &ltr35_info,
    &ltr25_info
};


static const char* f_usage_descr = \
"\nИспользование: ltrmodule-fpga-update [OPTIONS] firmware-file\n\n" \
" Утилита ltrmodule-fpga-update предназначана для обновления прошивок ПЛИС\n"
"   модулей LTR.\n"
" Всегда должено быть указано название типа модуля LTR с помощью опции \n"
"   -m,--module\n"
" Можно обновить все модули заданного типа, например (для LTR35):\n\n"
"   ltrmodule-fpga-update --module=LTR35 --all firmware-file\n\n"
" Также можно обновить прошивку только одного модуля:\n\n"
"   ltrmodule-fpga-update --module=LTR35 --csn=crate-serial --slot=slot-in-crate\n"
"                          fimware-file\n";

static const char* f_options_descr =
"Опции:\n" \
"    --all               - Выполнение операции для всех найденных модулей \n"
"                          указанного типа\n"
"-c, --csn=сер.номер     - Серийный номер крейта, для модулей которого будут \n"
"                          выполняться операции. Используется, если нужно\n"
"                          выполнить операцию для всех модулей только одного\n"
"                          крейта или для одного модуля, а крейтов несколько\n"
"-h, --help              - Print this help and exit\n"
"-m, --module=название   - Задает тип модуля. Должен быть равен названию одного\n"
"                          из поддерживаемых утилитой модулей\n"
"-o, --op=операция       - Выполняемая операция:\n"
"                            write  - запись прошивки из файла (с верификацией)\n"
"                            verify - только верификация\n"
"                            read   - чтение прошивки из флеш в файл\n"
"-s, --slot=слот         - Номер слота (от 1 до 16) модуля в крейте, если \n"
"                          обновление требуется только для одного модуля\n"
"-t, --type=тип          - Дополнительный спецификатор типа модуля, для \n"
"                          которого предназначена прошивка. Для некоторых \n"
"                          модулей обязателен (LTR25 - 'industrial' или \n"
"                          'commercial')";


static void  f_print_usage(void) {
    unsigned int i;
    fprintf(stdout, "%s", f_usage_descr);
    fprintf(stdout, "\nПоддерживаемые модули:\n");
    for (i=0; i < sizeof(f_modules)/sizeof(f_modules[0]); i++)  {
        fprintf(stdout, "    %s\n", f_modules[i]->name);
        if (f_modules[i]->type_specifiers!=NULL) {
             const char **spec = f_modules[i]->type_specifiers;
             while (*spec!=NULL) {
                 fprintf(stdout, "        %s\n", *spec);
                 spec++;
             }
        }
    }
    fprintf(stdout,"\n");
    fprintf(stdout, "%s", f_options_descr);
}



static int f_stricmp (const char *p1, const char *p2) {
    unsigned char *s1 = (unsigned char *) p1;
    unsigned char *s2 = (unsigned char *) p2;
    unsigned char c1, c2;

    do {
        c1 = (unsigned char) toupper((int)*s1++);
        c2 = (unsigned char) toupper((int)*s2++);
    }
    while ((c1 == c2) && (c1 != '\0'));

    return c1 - c2;
}

static int f_parse_options(t_ltrcfg_state* st, int argc, char **argv, int* out) {
    int err = 0;
    int opt = 0;

    *out = 0;
    st->csn = NULL;
    st->mod_type_spec = NULL;

    /************************** разбор опций *****************************/
    while ((opt!=-1) && !err && !*out) {
        opt = getopt_long(argc, argv, f_opt_str,
                          f_long_opt, 0);
        switch (opt) {
            case -1:
                break;
            case OPT_HELP:
                f_print_usage();
                *out = 1;
                break;
            case OPT_MODULE: {
                    unsigned i;
                    for (i=0; i < sizeof(f_modules)/sizeof(f_modules[0]); i++)  {
                        if (!f_stricmp(f_modules[i]->name, optarg)) {
                            st->module = f_modules[i];
                            break;
                        }
                    }
                    if (st->module==NULL) {
                        fprintf(stderr, "Задан неподдерживаемый тип модуля!\n");
                        err = ERR_INVALID_USAGE;
                    }
                }
                break;
            case OPT_MODULE_TYPE:
                st->mod_type_spec = optarg;
                break;
            case OPT_CRATE_SN:
                st->csn = optarg;
                break;
            case OPT_ALL:
                st->all = 1;
                break;
            case OPT_SLOT: {
                    int slot = atoi(optarg);
                    if ((slot < LTR_CC_CHNUM_MODULE1) || (slot > LTR_CC_CHNUM_MODULE16)) {
                        fprintf(stderr, "Неверно задан номер слота модуля!\n");
                        err = ERR_INVALID_USAGE;
                    } else {
                        st->slot = slot;
                    }
                }
                break;
            default:
                break;
        }
    }

    if (!err && !*out) {
        if (st->module==NULL) {
            fprintf(stderr, "Не задан тип модуля!\n");
            err = ERR_INVALID_USAGE;
        }

        if (optind >= argc) {
            fprintf(stderr, "Не задан файл с прошивкой ПЛИС\n");
            err = ERR_INVALID_USAGE;
        } else {
            st->filename = argv[optind];
        }

        if (!err && (st->module->flags & MODULE_FLAG_TYPE_SPEC_REQIRED)
                && (st->mod_type_spec==NULL)) {
            fprintf(stderr, "Для данного модуля должна быть задана модификация с помощью опции --type\n");
            err = ERR_INVALID_USAGE;
        }

        if (!err && (st->mod_type_spec!=NULL) && (st->module->type_specifiers!=NULL)) {
            int fnd = 0;
            unsigned pos;
            for (pos=0; !fnd && (st->module->type_specifiers[pos]!=NULL); pos++) {
                if (!strcmp(st->mod_type_spec, st->module->type_specifiers[pos])) {
                    fnd = 1;
                }
            }

            if (!fnd) {
                fprintf(stderr, "Указана неизвестная модификация модуля!\n");
                err = ERR_INVALID_USAGE;
            }
        }
    }
    return err;
}



static int f_write_firm(t_module_info *dev, const char* req_type_spec, const char *csn, INT slot, FILE* f, INT ops) {
    void *hdev=NULL;
    INT res = LTR_OK;
    DWORD cur_addr;
    BYTE *data=NULL, *data_ver=NULL;
    BOOL wr_enabled = FALSE;


    printf("\nМодуль %s: крейт %s, слот %d\n", dev->name, csn, slot);

    data = malloc(dev->flash_block_size);
    data_ver = malloc(dev->flash_block_size);
    if ((data==NULL) || (data_ver==NULL)) {
        res = LTR_ERROR_MEMORY_ALLOC;
        fprintf(stderr, "Ошибка выделения памяти!\n");
    }

    if (res==LTR_OK) {
        res = dev->open(&hdev, LTRD_ADDR_LOCAL, LTRD_PORT_DEFAULT, csn, slot);
        if (res!=LTR_OK) {
            fprintf(stderr, "Не удалось установить связь с модулем! Ошибка %d: %s\n",
                res, dev->err_str(hdev, res));
        }
    }

    if (res==LTR_OK) {
        if ((req_type_spec!=NULL) && (dev->type_spec!=NULL)) {
            const char *dev_type_spec = dev->type_spec(hdev);
            if ((dev_type_spec!=NULL) && strcmp(req_type_spec, dev_type_spec) ){
                printf("Неподходящая модификация модуля... пропуск.\n");
                res = ERR_MODULE_INVALID_TYPE;
            }
        }
    }

    if ((res==LTR_OK) && (ops & OP_WRITE)) {
        printf("Стирание...\n");
        res = dev->fpga_wr_en(hdev, TRUE);
        if (res!=LTR_OK) {
            fprintf(stderr, "Не удалось разрешить запись в область памяти с прошивкой! Ошибка %d: %s\n",
                   res, dev->err_str(hdev, res));
        } else {
            wr_enabled = TRUE;
        }

        if (res==LTR_OK)
            res = dev->erase(hdev, dev->fpga_firm_addr, dev->fpga_firm_size);
        if (res!=LTR_OK) {
            fprintf(stderr, "Не удалось стереть содиржимое прошивки! Ошибка %d: %s\n",
                   res, dev->err_str(hdev, res));
        }
    }

    if ((res==LTR_OK) && (ops & OP_WRITE)) {
        printf("Запись...\n");
        fseek(f, 0, SEEK_SET);
        if (res==LTR_OK) {
            INT rd_cnt = dev->flash_block_size;
            cur_addr = dev->fpga_firm_addr;

            while ((rd_cnt == (INT)dev->flash_block_size) && (res==LTR_OK)) {
                rd_cnt = (INT)fread(data, 1, dev->flash_block_size, f);
                if (rd_cnt > 0) {
                    INT i;
                    for (i=0; i < rd_cnt; i++) {
                        data[i] = ((data[i] & 0x0001) << 7) | ((data[i] & 0x0002) << 5)
                                | ((data[i] & 0x0004) << 3) | ((data[i] & 0x0008) << 1)
                                | ((data[i] & 0x0010) >> 1) | ((data[i] & 0x0020) >> 3)
                                | ((data[i] & 0x0040) >> 5) | ((data[i] & 0x0080) >> 7);
                    }
                    res = dev->write(hdev, cur_addr, data, rd_cnt);
                    if (res==LTR_OK) {
                        cur_addr+= rd_cnt;
                        printf(".");
                        fflush(stdout);
                    } else {
                        fprintf(stderr, "\nНе удалось выполнить запись во Flash-память! Ошибка %d: %s\n",
                                res, dev->err_str(hdev, res));
                    }
                } else if (rd_cnt < 0) {
                    res = -1;
                    fprintf(stderr, "\nОшибка чтения файла!\n");
                }
            }
        }

        if (res==LTR_OK)
            printf("\nОк.\n");
    }


    if (wr_enabled) {
        INT dis_res = dev->fpga_wr_en(hdev, FALSE);
        if (dis_res!=LTR_OK) {
            fprintf(stderr, "Не удалось запретить запись в область памяти с прошивкой! Ошибка %d: %s\n",
                   dis_res, dev->err_str(hdev, dis_res));

            if (res==LTR_OK)
                res = dis_res;
        }
    }



    if ((res==LTR_OK) && (ops & OP_VERIFY)) {
        INT rd_cnt;
        fseek(f, 0, SEEK_SET);
        printf("Верификация...\n");

        rd_cnt  = dev->flash_block_size;
        cur_addr = dev->fpga_firm_addr;

        while ((rd_cnt == (INT)dev->flash_block_size) && (res==LTR_OK)) {
            rd_cnt = (INT)fread(data, 1, dev->flash_block_size, f);
            if (rd_cnt > 0) {
                INT i;
                for (i=0; i < rd_cnt; i++) {
                    data[i] = ((data[i] & 0x0001) << 7) | ((data[i] & 0x0002) << 5)
                            | ((data[i] & 0x0004) << 3) | ((data[i] & 0x0008) << 1)
                            | ((data[i] & 0x0010) >> 1) | ((data[i] & 0x0020) >> 3)
                            | ((data[i] & 0x0040) >> 5) | ((data[i] & 0x0080) >> 7);
                }

                res = dev->read(hdev, cur_addr, data_ver, rd_cnt);
                if (res==LTR_OK) {
                    for (i=0; (i < rd_cnt) && (res==LTR_OK); i++) {
                        if (data[i]!=data_ver[i]) {
                            res = LTR_ERROR_FLASH_VERIFY;
                            fprintf(stderr, "\nОшибка при проверке записи! Адресс = 0x%06X, записано = 0x%02X, прочитано = 0x%02X\n",
                                    cur_addr+i, data[i], data_ver[i]);
                        }
                    }

                    if (res==LTR_OK) {
                        printf(".");
                        fflush(stdout);
                    }
                } else {
                    fprintf(stderr, "\nНе удалось выполнить чтение из Flash-памяти! Ошибка %d: %s\n",
                            res, dev->err_str(hdev, res));
                }
            } else if (rd_cnt < 0) {
                res = -1;
                fprintf(stderr, "\nОшибка чтения файла!\n");
            }

            if (res==LTR_OK) {
                cur_addr+= rd_cnt;
            }
        }

        if (res==LTR_OK) {
            printf("\nОк.\n");
        }
    }

    free(data);
    free(data_ver);

    if (hdev)
        dev->close(hdev);
    return res;
}

int main(int argc, char** argv) {
    t_ltrcfg_state st;
    int out, err;
#ifdef _WIN32
    setlocale(LC_ALL, "");
#endif

    memset(&st, 0, sizeof(st));

    /* разбор опций */
    err = f_parse_options(&st, argc, argv, &out);

    if (!err && !out) {
        FILE* f = fopen(st.filename, "rb");
        if (f==NULL) {
            err = ERR_OPEN_FILE;
            fprintf(stderr, "Не удалось открыть файл с прошивкой ПЛИС!\n");
        }

        if (!err) {
            INT modules_ok=0, modules_err = 0;
            /* устанавливаем управляющее соединение с сервером для получения
             * списка крейтов */
            TLTR hltr;


            LTR_Init(&hltr);            
            err = LTR_OpenSvcControl(&hltr, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
            if (err!=LTR_OK) {
                fprintf(stderr, "Не удалось установить соединение с сервером. Ошибка %d: %s!",
                        err, LTR_GetErrorString(err));
            } else {
                /* получаем список крейтов */
                char crates[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
                err = LTR_GetCrates(&hltr, (BYTE*)crates);
                if (err!=LTR_OK) {
                    fprintf(stderr, "Не удалось получить список крейтов. Ошибка %d: %s!",
                        err, LTR_GetErrorString(err));
                }

                if (err==LTR_OK) {
                    WORD i;
                    for (i=0; i < LTR_CRATES_MAX; i++) {
                        if ((crates[i][0]!=0) && ((st.csn==NULL) ||
                            !strcmp(crates[i], st.csn))) {
                            /* Если серийный крейта не задан - выполняем для
                             *  всех серийных номеров, иначе - только для
                             *  одного крейта */
                            TLTR hcrate;
                            INT cr_err = LTR_OK;

                            LTR_Init(&hcrate);
                            cr_err = LTR_OpenCrate(&hcrate, hltr.saddr, hltr.sport,
                                                   LTR_CRATE_IFACE_UNKNOWN, crates[i]);
                            if (cr_err) {
                                fprintf(stderr, "Не удалось открыть крейт с серийным номером = %s. Ошибка %d: %s!",
                                    crates[i], cr_err, LTR_GetErrorString(cr_err));
                            } else {
                                WORD mids[LTR_MODULES_PER_CRATE_MAX];
                                cr_err = LTR_GetCrateModules(&hcrate, mids);
                                if (cr_err) {
                                    fprintf(stderr, "Не удалось получить список модулей для крейта с номером %s. Ошибка %d: %s!",
                                        hcrate.csn, cr_err, LTR_GetErrorString(cr_err));
                                } else {
                                    /* ищем модули, для которых подходит прошивка */
                                    INT m;
                                    for (m=0; m < LTR_MODULES_PER_CRATE_MAX; m++) {
                                        if ((mids[m]==st.module->mid) && ((st.slot==0) ||
                                                                     (st.slot==(m+LTR_CC_CHNUM_MODULE1)))) {

                                            INT merr = f_write_firm(st.module, st.mod_type_spec, hcrate.csn,
                                                                    m+LTR_CC_CHNUM_MODULE1,
                                                                    f, OP_VERIFY | OP_WRITE);
                                            if (merr != ERR_MODULE_INVALID_TYPE) {
                                                if (merr) {
                                                    modules_err++;
                                                } else {
                                                    modules_ok++;
                                                }
                                            }
                                        }
                                    }
                                }
                                LTR_Close(&hcrate);
                            }
                        }
                    }

                    printf("\nОперация завершена!\n");
                    printf("   Обновлено прошивок модулей: %d, успешно: %d, с ошибками: %d\n", modules_ok+modules_err, modules_ok, modules_err);
                }

                LTR_Close(&hltr);

                if (!err && (modules_err!=0))
                    err = ERR_OPS_WITH_ERRORS;
            }
        }

        if (f!=NULL)
            fclose(f);
    }
    return err;
}
