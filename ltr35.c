#include "ltrmodule_fpga_update.h"
#include "ltr/include/ltr35api.h"
#include <stdlib.h>


static INT f_mod_open(void** hnd, DWORD net_addr, WORD net_port, const char *csn, INT slot);
static INT f_mod_close(void* hnd);
static INT f_mod_fpga_wr_en(void* hnd, BOOL en);
static INT f_mod_erase(void* hnd, DWORD addr, DWORD size);
static INT f_mod_write(void* hnd, DWORD addr, const BYTE* data, DWORD size);
static INT f_mod_read(void* hnd, DWORD addr, BYTE* data, DWORD size);
static LPCSTR f_mod_err_str(void *hnd, INT err);

extern LTR35API_DllExport(INT) LTR35_FPGAFirmwareWriteEnable(TLTR35 *hnd, BOOL en) ;
extern LTR35API_DllExport(INT) LTR35_FPGAHoldFlashIface(TLTR35 *hnd, BOOL en);


t_module_info ltr35_info = {
    "LTR35",
    LTR_MID_LTR35,
    0,
    0x002000,
    0x040000 - 0x002000,
    8*1024,

    NULL,
    f_mod_open,
    f_mod_close,
    f_mod_fpga_wr_en,
    f_mod_erase,
    f_mod_write,
    f_mod_read,
    f_mod_err_str,
    NULL
};




static INT f_mod_open(void** phnd, DWORD net_addr, WORD net_port, const char *csn, INT slot) {
    TLTR35* hnd = malloc(sizeof(TLTR35));
    INT err = hnd==NULL ? LTR_ERROR_MEMORY_ALLOC : LTR_OK;

    if (err==LTR_OK) {
        LTR35_Init(hnd);
        err = LTR35_Open(hnd, net_addr, net_port, csn, slot);
        /* Игнорируем некритические ошибки, которые нам все равно могут дать
         * обновить прошивку */
        if ((err != LTR_WARNING_MODULE_IN_USE) && (LTR35_IsOpened(hnd)==LTR_OK)) {
            err = LTR35_FPGAEnable(hnd, FALSE);
            if (err == LTR_OK)
                err = LTR35_FPGAHoldFlashIface(hnd, TRUE);
        }
    }

    if (err==LTR_OK) {
        *phnd = hnd;
    } else {
        free(hnd);
    }
    return err;
}

static INT f_mod_close(void* hnd) {
    LTR35_FPGAHoldFlashIface(hnd, FALSE);
    INT err = LTR35_Close((TLTR35*)hnd);
    free(hnd);
    return err;
}

static INT f_mod_fpga_wr_en(void* hnd, BOOL en) {
    return LTR35_FPGAFirmwareWriteEnable((TLTR35*)hnd, en);
}

static INT f_mod_erase(void* hnd, DWORD addr, DWORD size) {
    return LTR35_FlashErase((TLTR35*)hnd, addr, size);
}

static INT f_mod_write(void* hnd, DWORD addr, const BYTE* data, DWORD size) {
    return LTR35_FlashWrite((TLTR35*)hnd, addr, data, size, LTR35_FLASH_WRITE_ALREDY_ERASED);
}

static INT f_mod_read(void* hnd, DWORD addr, BYTE* data, DWORD size) {
    return LTR35_FlashRead((TLTR35*)hnd, addr, data, size);
}

static LPCSTR f_mod_err_str(void *hnd, INT err) {
    return LTR35_GetErrorString(err);
}
