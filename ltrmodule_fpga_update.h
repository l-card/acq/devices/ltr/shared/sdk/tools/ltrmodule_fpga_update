#ifndef LTRMODULE_FPGA_UPDATE_H
#define LTRMODULE_FPGA_UPDATE_H


#include "ltr/include/ltrapi.h"

typedef INT (*mod_open)(void** hnd, DWORD net_addr, WORD net_port, const char *csn, INT slot);
typedef INT (*mod_close)(void* hnd);
typedef INT (*mod_fpga_wr_en)(void* hnd, BOOL en);
typedef INT (*mod_erase)(void* hnd, DWORD addr, DWORD size);
typedef INT (*mod_write)(void* hnd, DWORD addr, const BYTE* data, DWORD size);
typedef INT (*mod_read)(void* hnd, DWORD addr, BYTE* data, DWORD size);
typedef LPCSTR (*mod_err_str)(void *hnd, INT err);
typedef LPCSTR (*mod_type_spec)(void *hnd);


typedef enum {
    MODULE_FLAG_TYPE_SPEC_REQIRED = 0x01
} t_module_flags;


typedef struct {
    const char *name;
    WORD  mid;
    WORD  flags;
    DWORD fpga_firm_addr;
    DWORD fpga_firm_size;
    DWORD flash_block_size;

    const char**    type_specifiers;

    mod_open        open;
    mod_close       close;
    mod_fpga_wr_en  fpga_wr_en;
    mod_erase       erase;
    mod_write       write;
    mod_read        read;
    mod_err_str     err_str;
    mod_type_spec   type_spec;
} t_module_info;







#endif // LTRMODULE_FPGA_UPDATE_H
